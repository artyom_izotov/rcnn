#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/photo.hpp>
#include <opencv2/xphoto.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/saliency.hpp>
#include <opencv2/bioinspired.hpp>
#include <opencv2/objdetect.hpp>

#include <boost/filesystem.hpp>

#include "caffe/caffe.hpp"
#include <google/protobuf/text_format.h>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace caffe;
using namespace cv;
using namespace cv::saliency;
using namespace cv::bioinspired;
using namespace cv::ximgproc;
using namespace cv::xphoto;
using namespace boost::filesystem;

using namespace std;

//string to double
#define _F(X) (float)atoi(X.c_str())

struct bbox //bounding box
{
    int   idx;
    float obj_score;
    float caffe_score;
    float caffe_class;
    float area;
    float ratio;
    Vec4i rect;
    vector<float> prob;
};

bool cmp_caffe_score_ascend(bbox bb1, bbox bb2) {
    return bb1.caffe_score < bb2.caffe_score;
}//good
bool cmp_caffe_score_descend(bbox bb1, bbox bb2) {
    return bb1.caffe_score > bb2.caffe_score;
}//nms
bool cmp_obj_score_ascend(bbox bb1, bbox bb2) {
    return bb1.obj_score < bb2.obj_score;
}//good
bool cmp_obj_score_descend(bbox bb1, bbox bb2) {
    return bb1.obj_score > bb2.obj_score;
}//nms
bool cmp_class_eq(bbox bb1, bbox bb2) {
    return bb1.caffe_class == bb2.caffe_class;
}//useless

bool BING(const Mat&, vector<bbox>&, int); //generate proposals
Blob<float>* batchToBlob(const Mat&, const vector<bbox>&, const Blob<float>&, int); //convert single batch to Blob object
Mat makeThumb(const Mat&, int);
bool ReadMatToDatum(const Mat&, const int, Datum*);
void getVOCBBoxes(path, int, vector< vector<bbox> >&, vector<string>& );
void _nms(vector<bbox>&, vector<bbox>&, float);
void vectorToBatches(Mat&, Blob<float>&, vector<bbox>&, vector<Blob<float>*>&, int&);
void getCaffeClasses(Net<float>&, Mat&, Blob<float>&, vector<bbox>&);
void loadTestImages(path, vector<string>&, int);
template< typename T > string toString(T);

double _int_uni(Vec4i bb, Vec4i bb_gt)
{
    Vec4i bi;
    bi[0] = max(bb[0], bb_gt[0]);
    bi[1] = max(bb[1], bb_gt[1]);
    bi[2] = min(bb[2], bb_gt[2]);
    bi[3] = min(bb[3], bb_gt[3]);

    double iw = bi[2] - bi[0] + 1;
    double ih = bi[3] - bi[1] + 1;
    double ov = 0;
    if (iw>0 && ih>0){
        double ua = (bb[2]-bb[0]+1)*(bb[3]-bb[1]+1)+(bb_gt[2]-bb_gt[0]+1)*(bb_gt[3]-bb_gt[1]+1)-iw*ih;
        ov = iw*ih/ua;
    }
    return ov;
}

int main()
{
    //set operating mode
    Caffe::set_phase(Caffe::TEST);
    Caffe::set_mode(Caffe::GPU);

    //init caffe
    Net<float> caffe_net("data/7cat_deploy.prototxt");
    caffe_net.CopyTrainedLayersFrom("data/7cat_deploy.caffemodel");

    //
    //load binary mean file
    cout << "loading mean...\n";
    Blob<float> blob_mean;
    BlobProto blob_proto_mean;
    ReadProtoFromBinaryFileOrDie("data/imagenet_mean.binaryproto", &blob_proto_mean);
    blob_mean.FromProto(blob_proto_mean);
    cout << "mean loaded...\n";

    //description parameters
    int im_context = 16;
    int datum_c = 3;
    int datum_w = 227;
    int datum_h = 227;
    int chunk_size = datum_c*datum_w*datum_h;
    int num_chunks = 2500;
    int batch_size = 250;
    int num_batches = num_chunks/batch_size;
    int chunk_stride = (blob_mean.height() - datum_h) / 2;
    //
    Datum datum;
    datum.set_channels(datum_c);
    datum.set_width(datum_w);
    datum.set_height(datum_h);
    cout << "datum allocated...\n";

    Blob<float>* blob = new Blob<float>(batch_size, datum_c, datum_h, datum_w);

    BlobProto blob_proto;
    blob_proto.set_num(batch_size);
    blob_proto.set_channels(datum_c);
    blob_proto.set_height(datum_h);
    blob_proto.set_width(datum_w);
    for (int i = 0; i < batch_size*chunk_size; ++i) {
        blob_proto.add_data(0.);
    }

    //
    vector<string> im_files;
    loadTestImages((path)"app-7cat/real_data_test/0furniture", im_files, 15);
    //loadTestImages((path)"fashionista", im_files, 50)
    //loadTestImages((path)"images_test", im_files, 9);
    int cnt = 0;
    /*
    for(float _grev = 0.3; _grev < 1; _grev += 0.3)
    {
        for(float _cv = 0.1; _cv < 1; _cv += 0.3)
        {
            for(float _nmsv = 0.1; _nmsv < 1; _nmsv += 0.3)
            {
                for(int _grv = 3; _grv < 10; _grv+=3)
                {


                    cout << cnt << " | caffe : " << _cv << ", nms : " << _nmsv << ", gR (rects) : " << _grv << ", gR  (eps) : " << _grev << "\n";
                    //cout << cnt << " | caffe : " << _cv << ", nms : - " <<  ", gR (rects) : " << _grv << ", gR  (eps) : " << _grev << "\n";
       */
    float _cv = 0.1;
    float _nmsv = 0.5;
    int _grv = 4;
    float _grev = 0.6;

    vector<Mat> _src_im;
    for(int f = 0; f<im_files.size(); f++)
    {
        //load image
        Mat im, im_raw = imread(im_files[f]);
        im = makeThumb(im_raw, 400);

        //
        vector<bbox> chunks;
        BING(im, chunks, num_chunks);

        chunks.pop_back();
        bbox _whole_im;
        _whole_im.rect = Vec4i(0, 0, im.cols, im.rows);

        vector<Blob<float>*> blobs;
        for(int p = 0; p<num_batches; p++)
        {
            for(int k = 0; k<batch_size*chunk_size; k++)
            {
                blob_proto.set_data(k, 0.);
            }
            for(int k = 0; k<batch_size; k++)
            {

                //pad window proposal
                int pad_x1 = max((float)chunks[k + p*batch_size].rect[0] - im_context, (float)0);
                int pad_y1 = max((float)chunks[k + p*batch_size].rect[1] - im_context, (float)0);
                int pad_x2 = min((float)chunks[k + p*batch_size].rect[2] + im_context, (float)im.cols-1);
                int pad_y2 = min((float)chunks[k + p*batch_size].rect[3] + im_context, (float)im.rows-1);

                Mat proposal = Mat::zeros(pad_y2 - pad_y1, pad_x2 - pad_x1, im.type());
                im(Rect(Point2i(pad_x1, pad_y1), Point2i(pad_x2, pad_y2))).copyTo(proposal);
                Mat im_chunk = Mat::zeros(datum_h, datum_w, CV_8UC3);
                im_chunk = makeThumb(proposal, 227);

                //imshow("chunk", im_chunk);
                //waitKey(0);
                if (!ReadMatToDatum(im_chunk, 0, &datum))
                {
                    std::cerr << "Cannot transform " << k << "'th image chunk to datum..." << std::endl;
                }
                //fill BlobProto
                const string& data = datum.data();
                if (data.size() != 0)
                {
                    for (int i = 0; i < chunk_size; ++i)
                    {
                        blob_proto.set_data(i + k*chunk_size, blob_proto.data(i + k*chunk_size) + (float)((uchar)data[i]));
                    }
                }
            }
            //cout << "blob_proto filled...\n";
            //subtract mean
            blob->FromProto(blob_proto);
            //cout << "blob_proto sent to blob ...\n";
            for(int k = 0; k<batch_size; k++)
            {
                float* data_vec = blob[0].mutable_cpu_data() + blob[0].offset(k);
                for (int c = 0; c < datum_c; ++c)
                {
                    for (int h = 0; h < datum_h; ++h)
                    {
                        for (int w = 0; w < datum_w; ++w)
                        {
                            data_vec[(c*datum_h + h)*datum_w + w] -= blob_mean.data_at(0, c, h + chunk_stride, w + chunk_stride);
                        }
                    }
                }
                //
            }
            blobs.push_back(blob);
        }
        //
        int layer_size;
        for(int p = 0; p<blobs.size(); p++)
        {
            float loss = 0.0;
            vector<Blob<float>*> bottom;
            bottom.push_back(blobs[p]);
            caffe_net.Forward(bottom, &loss);
            const shared_ptr<Blob<float> > feature_blob = caffe_net.blob_by_name("prob"); //get the last layer
            layer_size = feature_blob->count()/feature_blob->num();
            for(int k = 0; k<batch_size; k++)
            {
                int idx = p*batch_size + k;
                float max_p = 0;
                float max_c = 0;
                const float* res_ = feature_blob->cpu_data() + feature_blob->offset(k);
                for(int i = 0; i<layer_size; i++)
                {
                    const float value = res_[i];
                    //chunks[idx].prob.push_back(value);
                    if (max_p < value)
                    {
                        max_p = value;
                        max_c = i;
                    }
                }
                chunks[idx].caffe_score = max_p;
                chunks[idx].caffe_class = max_c;
                //

                //if((max_p > 0.4) && (chunks[idx].area > 1000)/*  && (chunks[idx].ratio < 3.5)*/)
                //{
                //    c_bbox.push_back(chunks[idx]);
                //}
            }

        }
        vector< vector<bbox> > _prop, _prop_sup2, _prop_sup3, _prop_sup4, _prop_sup;
        for(int i = 0; i<layer_size; i++)
        {
            _prop.push_back(vector<bbox>());
            _prop_sup.push_back(vector<bbox>());
            _prop_sup2.push_back(vector<bbox>());
            _prop_sup3.push_back(vector<bbox>());
            _prop_sup4.push_back(vector<bbox>());
        }
        vector<bbox> prop, reg;
        for(int i = 0; i<chunks.size(); i++)
        {
            Rect rect;
            rect.x = chunks[i].rect[0];
            rect.y = chunks[i].rect[1];
            rect.width = chunks[i].rect[2]-chunks[i].rect[0];
            rect.height = chunks[i].rect[3]-chunks[i].rect[1];
            //rectangle(im, rect, Scalar(0,0,255),1);

            if(chunks[i].caffe_score > 0.4/* && chunks[i].caffe_class == 2*/)
            {
                _prop[ chunks[i].caffe_class ].push_back(chunks[i]);
                prop.push_back(chunks[i]);
                //rectangle(im, rect, Scalar(rand()%255,rand()%255,rand()%255),1);
            }

            //cout << chunks[i].idx << " " << chunks[i].caffe_class << " " << chunks[i].caffe_score << " | ";
            //for(int k = 0; k<chunks[i].prob.size(); k++) cout << chunks[i].prob[k] << " ";
            //cout << "\n";


        }
        //per-class NMS
        vector<Scalar> colors;
        colors.push_back(Scalar(0,0,255)); //0
        colors.push_back(Scalar(0,255,0)); //1
        colors.push_back(Scalar(255,0,0)); //2
        colors.push_back(Scalar(0,255,255)); //3
        colors.push_back(Scalar(255,255,0));
        colors.push_back(Scalar(255,0,255));
        colors.push_back(Scalar(255,255,255));
        for(int i = 0; i<_prop.size(); i++)
        //for(int i = 0; i<2; i++)
        //{
        //int i = 6;
        {

            _nms(_prop[i], _prop_sup2[i], 0.4);
            _nms(_prop_sup2[i], _prop_sup[i], 0.6);
            //_nms(_prop_sup2[i], _prop_sup[i], 0.5);
            //_nms(_prop_sup3[i], _prop_sup[i], 0.2);
            //_nms(_prop_sup4[i], _prop_sup[i], 0.1);
            vector<Rect> _tbb;
            for(int k = 0; k<_prop_sup[i].size(); k++)
            {                
                Rect rect;
                rect.x = _prop_sup[i][k].rect[0];
                rect.y = _prop_sup[i][k].rect[1];
                rect.width =  _prop_sup[i][k].rect[2] - _prop_sup[i][k].rect[0];
                rect.height = _prop_sup[i][k].rect[3] - _prop_sup[i][k].rect[1];
                _tbb.push_back(rect);
                //if(i == 1) rectangle(im, rect, colors[i], 1);
                //string str = toString(_prop_sup[i][k].caffe_score);
                //putText(im, str, Point(rect.x, rect.y), FONT_HERSHEY_COMPLEX_SMALL, 1.0, colors[i], 1);
            }

            /*
                            vector<Rect> _tbb;
                            for(int k = 0; k<_prop[i].size(); k++)
                            {
                                Rect rect;
                                rect.x = _prop[i][k].rect[0];
                                rect.y = _prop[i][k].rect[1];
                                rect.width =  _prop[i][k].rect[2] - _prop[i][k].rect[0];
                                rect.height = _prop[i][k].rect[3] - _prop[i][k].rect[1];
                                _tbb.push_back(rect);
                                rectangle(im, rect, colors[i], 1);
                            }
*/
            groupRectangles(_tbb, 6, 0.6);
            //Scalar color = Scalar(i*25, i*25, i*25);

            Scalar color = Scalar(0,0,255);
            for(int k = 0; k<_tbb.size(); k++)
            {
                rectangle(im, _tbb[k], colors[i], 2);
                string str = toString(i);
                putText(im, str, Point(_tbb[k].x, _tbb[k].y), FONT_HERSHEY_COMPLEX_SMALL, 1.0, colors[i], 1);
            }
        }


        /*
        //reg = prop;
        cout << reg.size() << "\n";
        vector<Rect> u_prop, g_prop;
        for(int k = 0; k<reg.size(); k++)
        {
            Rect rect;
            rect.x = reg[k].rect[0];
            rect.y = reg[k].rect[1];
            rect.width = reg[k].rect[2]  - reg[k].rect[0];
            rect.height = reg[k].rect[3] - reg[k].rect[1];
            rectangle(im, rect, Scalar(0,0,255),1);
            u_prop.push_back(rect);
        }
        groupRectangles(u_prop, 4, 0.8);
        for(int i = 0; i<u_prop.size(); i++)
        {
            rectangle(im, u_prop[i], Scalar(0,255,0),2);
        }
        */
        Mat r_im;
        resize(im, r_im, Size(500, 500));
        _src_im.push_back(r_im);

        imshow("im", r_im);
        if(waitKey() == 27) break;
        //waitKey();
    }
    /*
    cout << _src_im.size() << "\n";
    Mat s_im = Mat::zeros(3*227, 3*227, CV_8UC3);
    _src_im[0].copyTo( s_im(Rect(0, 0, 227, 227)) );
    _src_im[1].copyTo( s_im(Rect(227, 0, 227, 227)) );
    _src_im[2].copyTo( s_im(Rect(2*227, 0, 227, 227)) );

    _src_im[3].copyTo( s_im(Rect(0, 227, 227, 227)) );
    _src_im[4].copyTo( s_im(Rect(227, 227, 227, 227)) );
    _src_im[5].copyTo( s_im(Rect(2*227, 227, 227, 227)) );

    _src_im[6].copyTo( s_im(Rect(0, 2*227, 227, 227)) );
    _src_im[7].copyTo( s_im(Rect(227, 2*227, 227, 227)) );
    _src_im[8].copyTo( s_im(Rect(2*227, 2*227, 227, 227)) );

    std::stringstream ss;
    ss << cnt;

    std::string str;
    ss >> str;
    string filename = toString(cnt) + " | caffe : " + toString(_cv) + ", nms : " + toString(_nmsv) + ", gR (rects) : " + toString(_grv) + ", gR  (eps) : " + toString(_grev);
    //string filename = toString(cnt) + " | caffe : " + toString(_cv) + ", nms : - " + ", gR (rects) : " + toString(_grv) + ", gR  (eps) : " + toString(_grev);
    imwrite(filename + ".jpg", s_im);
    cout << "image written!\n";
    */
    cnt++;

    /*          }
            }
        }
    }
*/
    return 0;
}
template< typename T > string toString(T val)
{
    string str = static_cast<ostringstream*>( &(ostringstream() << val) )->str();
    return str;
}
void loadTestImages(path src, vector<string>& dst, int data_size)
{
    directory_iterator end_itr;
    //create list of all files
    int cnt = 0;
    for ( directory_iterator itr( src ); itr != end_itr; ++itr )
    {
        string _addr = itr->path().string();
        Mat _test_im = imread(_addr);
        if(_test_im.data)
        {
            dst.push_back(_addr);
        }
    }
    random_shuffle(dst.begin(), dst.end());
    dst.resize(data_size);
}

void getCaffeClasses(Net<float>& caffe_net, Mat& im, Blob<float>& blob_mean, vector<bbox>& chunks)
{
    //split vector of proposals to batches
    vector<Blob<float>*> blobs;
    int to_trim = 0;
    vectorToBatches(im, blob_mean, chunks, blobs, to_trim);
    cout << "to trim : " << to_trim << "\n";
    //
    int batch_size = 250;
    int num_batches = chunks.size()/250;
    for(int p = 0; p<blobs.size(); p++)
    {
        float loss = 0.0;
        vector<Blob<float>*> bottom;
        bottom.push_back(blobs[p]);
        caffe_net.Forward(bottom, &loss);
        const shared_ptr<Blob<float> > feature_blob = caffe_net.blob_by_name("prob"); //get the last layer
        int layer_size = feature_blob->count()/feature_blob->num();
        for(int k = 0; k<batch_size; k++)
        {
            float max_p = 0;
            float max_c = 0;
            const float* res_ = feature_blob->cpu_data() + feature_blob->offset(k);
            for(int i = 0; i<layer_size; i++)
            {
                const float value = res_[i];

                if (max_p < value)
                {
                    max_p = value;
                    max_c = i;
                }
            }
            int idx = p*batch_size + k;
            chunks[idx].caffe_score = max_p;
            chunks[idx].caffe_class = max_c;
            //

            //if((max_p > 0.4) && (chunks[idx].area > 1000)/*  && (chunks[idx].ratio < 3.5)*/)
            //{
            //    c_bbox.push_back(chunks[idx]);
            //}
        }

    }
    if(to_trim!=0)
    {
        chunks.resize(batch_size - to_trim);
    }
    while(blobs.size()!=0) blobs.pop_back();
}

void vectorToBatches(Mat& im, Blob<float>& blob_mean, vector<bbox>& chunks, vector<Blob<float>*>& dst, int& to_trim)
{
    int im_context = 16;
    int datum_c = 3;
    int datum_w = 227;
    int datum_h = 227;
    int chunk_size = datum_c*datum_w*datum_h;

    int num_chunks = chunks.size();

    int batch_size;
    int num_batches = 10; // :(

    if(num_chunks>400)
    {
        //TO DO
        batch_size = 250;
        int rem = num_chunks % batch_size;
        if(rem>batch_size/2 && rem!=0)
        {
            chunks.resize(num_chunks + batch_size - rem);
        }else if(rem<batch_size/2 && rem!=0){
            chunks.resize(num_chunks - rem);
        }

        int num_chunks = chunks.size();
        int num_batches = num_chunks/batch_size;
    }else{
        to_trim = 250 - chunks.size();
        chunks.resize(250);
        batch_size = chunks.size();
        num_batches = 1;
    }
    int chunk_stride = (blob_mean.height() - datum_h) / 2;
    //
    Datum datum;
    datum.set_channels(datum_c);
    datum.set_width(datum_w);
    datum.set_height(datum_h);
    cout << "datum allocated...\n";

    Blob<float>* blob = new Blob<float>(batch_size, datum_c, datum_h, datum_w);

    BlobProto blob_proto;
    blob_proto.set_num(batch_size);
    blob_proto.set_channels(datum_c);
    blob_proto.set_height(datum_h);
    blob_proto.set_width(datum_w);
    for (int i = 0; i < batch_size*chunk_size; ++i) {
        blob_proto.add_data(0.);
    }

    for(int p = 0; p<num_batches; p++)
    {
        for(int k = 0; k<batch_size*chunk_size; k++)
        {
            blob_proto.set_data(k, 0.);
        }
        for(int k = 0; k<batch_size; k++)
        {

            //pad window proposal
            int pad_x1 = max((float)chunks[k + p*batch_size].rect[0] - im_context, (float)0);
            int pad_y1 = max((float)chunks[k + p*batch_size].rect[1] - im_context, (float)0);
            int pad_x2 = min((float)chunks[k + p*batch_size].rect[2] + im_context, (float)im.cols-1);
            int pad_y2 = min((float)chunks[k + p*batch_size].rect[3] + im_context, (float)im.rows-1);

            Mat proposal = Mat::zeros(pad_y2 - pad_y1, pad_x2 - pad_x1, im.type());
            im(Rect(Point2i(pad_x1, pad_y1), Point2i(pad_x2, pad_y2))).copyTo(proposal);
            Mat im_chunk = Mat::zeros(datum_h, datum_w, CV_8UC3);
            im_chunk = makeThumb(proposal, 227);

            //imshow("chunk", im_chunk);
            //waitKey(0);
            if (!ReadMatToDatum(im_chunk, 0, &datum))
            {
                std::cerr << "Cannot transform " << k << "'th image chunk to datum..." << std::endl;
            }
            //fill BlobProto
            const string& data = datum.data();
            if (data.size() != 0)
            {
                for (int i = 0; i < chunk_size; ++i)
                {
                    blob_proto.set_data(i + k*chunk_size, blob_proto.data(i + k*chunk_size) + (float)((uchar)data[i]));
                }
            }
        }
        //cout << "blob_proto filled...\n";
        //subtract mean
        blob->FromProto(blob_proto);
        //cout << "blob_proto sent to blob ...\n";
        for(int k = 0; k<batch_size; k++)
        {
            float* data_vec = blob[0].mutable_cpu_data() + blob[0].offset(k);
            for (int c = 0; c < datum_c; ++c)
            {
                for (int h = 0; h < datum_h; ++h)
                {
                    for (int w = 0; w < datum_w; ++w)
                    {
                        data_vec[(c*datum_h + h)*datum_w + w] -= blob_mean.data_at(0, c, h + chunk_stride, w + chunk_stride);
                    }
                }
            }
            //
        }
        dst.push_back(blob);
    }
}
void getVOCBBoxes(path src, int count, vector< vector<bbox> >& dst, vector<string>& im_src)
{
    directory_iterator end_itr;
    vector<string> im_files;
    //create list of all files
    int cnt = 0;
    for ( directory_iterator itr( src ); itr != end_itr; ++itr )
    {
        if(cnt<3*count)
        {
            im_files.push_back( itr->path().string() );
            cnt++;
        }else{
            break;
        }
    }
    random_shuffle ( im_files.begin(), im_files.end() );
    cout << im_files.size() << "\n";
    //process files
    cnt = 0;
    string s_xmin, s_ymin, s_xmax, s_ymax;
    FileStorage fs;
    for(int k = 0; k<im_files.size(); k++)
    {
        if(cnt<count)
        {
            //read file
            fs.open(im_files[k], FileStorage::READ);
            FileNode fn = fs["annotation"]["object"];

            //get name of the image
            string filename;
            fs["annotation"]["filename"] >> filename;
            cout << im_files[k] << "...";

            //if img exists
            path im_path = "VOCdevkit/VOC2007/JPEGImages/"+filename;
            if(exists(im_path))
            {
                im_src.push_back(im_path.string());
                //get bounding boxes
                if (fn.isSeq())
                {
                    vector<bbox> _bbox;
                    for (FileNodeIterator it = fn.begin(), it_end = fn.end(); it != it_end; it++)
                    {
                        (*it)["bndbox"]["xmin"] >> s_xmin;
                        (*it)["bndbox"]["ymin"] >> s_ymin;
                        (*it)["bndbox"]["xmax"] >> s_xmax;
                        (*it)["bndbox"]["ymax"] >> s_ymax;
                        bbox _curr;
                        _curr.rect = Vec4i( _F(s_xmin),_F(s_ymin),_F(s_xmax),_F(s_ymax) );
                        _bbox.push_back(_curr);
                    }
                    dst.push_back(_bbox);
                }else{
                    vector<bbox> _bbox;
                    fn["bndbox"]["xmin"] >> s_xmin;
                    fn["bndbox"]["ymin"] >> s_ymin;
                    fn["bndbox"]["xmax"] >> s_xmax;
                    fn["bndbox"]["ymax"] >> s_ymax;
                    bbox _curr;
                    _curr.rect = Vec4i( _F(s_xmin),_F(s_ymin),_F(s_xmax),_F(s_ymax) );
                    _bbox.push_back(_curr);
                    dst.push_back(_bbox);
                }
                cout << " parsed.\n";
                cnt++;
            }else{ //skip if no image found
                cout << " couldn't be parsed - no image found.\n";
            }
            fs.release();

        }else{ //if we found all available image files - stop
            break;
        }
    }
}

bool BING(const Mat& im_src, vector<bbox>& obj_prop, int num_prop)
{
    //generate proposals
    vector<Vec4i> bbox_raw;
    Ptr<Saliency> sal_alg = Saliency::create( "BING" );
    sal_alg.dynamicCast<ObjectnessBING>()->setTrainingPath( "ObjectnessTrainedModel/" );
    sal_alg.dynamicCast<ObjectnessBING>()->computeSaliency( im_src, bbox_raw );
    vector<float> objectness = sal_alg.dynamicCast<ObjectnessBING>()->getobjectnessValues();
    //fill data structures
    for(int i = 0; i<bbox_raw.size(); i++)
    {
        float _width = bbox_raw[i][2] - bbox_raw[i][0];
        float _height = bbox_raw[i][3] - bbox_raw[i][1];
        bbox curr_bbox;
        curr_bbox.idx   = i;
        curr_bbox.obj_score = objectness[i];
        curr_bbox.caffe_score = 0;
        curr_bbox.caffe_class = -1;
        curr_bbox.rect = Vec4i(bbox_raw[i][0],bbox_raw[i][1],bbox_raw[i][2],bbox_raw[i][3]);
        curr_bbox.area  = (_width+1) * (_height+1);
        curr_bbox.ratio = max(_width, _height)/min(_width, _height);
        obj_prop.push_back(curr_bbox);
    }
    //sort and get first 2500 boxes
    sort(obj_prop.begin(), obj_prop.end(), cmp_obj_score_ascend);
    obj_prop.resize(num_prop);
    for(int i = 0; i<obj_prop.size(); i++) obj_prop[i].idx = i;
    return true;
}
bool ReadMatToDatum(const Mat & cv_img, const int label, Datum* datum)
{
    // accept only char type matrices
    CV_Assert(cv_img.depth() != sizeof(uchar));
    if (!cv_img.data) {
        return false;
    }

    const unsigned int num_channels = cv_img.channels();
    const unsigned int height = cv_img.rows;
    const unsigned int width = cv_img.cols;

    datum->set_channels(num_channels);
    datum->set_height(height);
    datum->set_width(width);
    datum->set_label(label);
    datum->clear_data();
    datum->clear_float_data();
    string* datum_string = datum->mutable_data();

    for (unsigned int c=0; c < num_channels; ++c) {
        for (unsigned int h = 0; h < height; ++h){
            const Vec3b *cv_img_data = cv_img.ptr<Vec3b>(h);
            for (unsigned int w = 0; w < width; ++w){
                datum_string->push_back(static_cast<char>(cv_img_data[w][c]));
            }
        }
    }

    return true;
}
Mat makeThumb(const Mat &in_img, int thumb_size)
{
    Mat img_cropped, img_resized;
    img_cropped = in_img.clone();

    Mat I_thumb(thumb_size,thumb_size,CV_8UC3,Scalar(255,255,255));
    Rect roi;
    float origAspect = (img_cropped.cols / (float)img_cropped.rows);
    if (origAspect > 1)
    {
        cv::resize(img_cropped,img_resized,cv::Size(thumb_size,floor(thumb_size/origAspect)),0,0,cv::INTER_AREA);
        cv::Size resSize = img_resized.size();
        int padding = floor((thumb_size-resSize.height) / 2.0);
        roi = cv::Rect(0,padding,thumb_size,resSize.height);
    }
    else
    {
        cv::resize(img_cropped,img_resized,cv::Size(floor(thumb_size*origAspect),thumb_size),0,0,cv::INTER_AREA);
        cv::Size resSize = img_resized.size();
        int padding = floor((thumb_size-resSize.width) / 2.0);
        roi = cv::Rect(padding,0,resSize.width,thumb_size);
    }

    cv::Mat roiImg = I_thumb(roi);
    img_resized.copyTo(roiImg);
    return I_thumb;
}
void _nms(vector<bbox> &src, vector<bbox> &dst, float overlap)
{
    /*
if isempty(boxes)
  pick = [];
else
  x1 = boxes(:,1);
  y1 = boxes(:,2);
  x2 = boxes(:,3);
  y2 = boxes(:,4);
  s = boxes(:,end);
  area = (x2-x1+1) .* (y2-y1+1);

  [vals, I] = sort(s);
  pick = [];
  while ~isempty(I)
    last = length(I);
    i = I(last);
    pick = [pick; i];
    suppress = [last];
    for pos = 1:last-1
      j = I(pos);
      xx1 = max(x1(i), x1(j));
      yy1 = max(y1(i), y1(j));
      xx2 = min(x2(i), x2(j));
      yy2 = min(y2(i), y2(j));
      w = xx2-xx1+1;
      h = yy2-yy1+1;
      if w > 0 && h > 0
        % compute overlap
        o = w * h / area(i); %%%% j
        if o >= overlap
          suppress = [suppress; pos];
        end
      end
    end
    I(suppress) = [];
  end
end
    */

    vector<bbox> src_(src);
    vector<int> x1, x2, y1, y2, I, pick;
    vector<float> area;
    for(int i = 0; i<src_.size(); i++)
    {
        x1.push_back(src_[i].rect[0]);
        y1.push_back(src_[i].rect[1]);
        x2.push_back(src_[i].rect[2]);
        y2.push_back(src_[i].rect[3]);
        area.push_back((float)((x2[i] - x1[i] + 1) * (y2[i] - y1[i] + 1)));
        src_[i].idx = i;
    }
    sort(src_.begin(), src_.end(), cmp_caffe_score_descend);
    for(int i = 0; i<src_.size(); i++)
    {
        I.push_back(src_[i].idx);
        //I.push_back(i);
   }
    while(I.size()!=0)
    {
        int last = I.size() - 1;
        int i = I[last];
        pick.push_back(i);
        vector<int> suppress;
        suppress.push_back(last);
        for(int pos = 0; pos<last - 1; pos++)
        {
            int j = I[pos];
            int xx1 = max(x1[i], x1[j]);
            int yy1 = max(y1[i], y1[j]);
            int xx2 = min(x2[i], x2[j]);
            int yy2 = min(y2[i], y2[j]);
            float w = xx2 - xx1 + 1;
            float h = yy2 - yy1 + 1;
            if(w>0 && h>0)
            {
                float o = w*h/area[i];
                if(o>=overlap)
                {
                    suppress.push_back(pos);
                }
            }
        }
        for(int k = 0; k<suppress.size(); k++)
        {
            I[suppress[k]] = -1;
        }
        I.erase(remove(I.begin(), I.end(), -1), I.end());
        //cout << I.size() << "\n";
    }
    /*
    for(int i = 0; i<src_.size(); i++)
    {

        bool is_eq = false;

        for(int k = 0; k<pick.size(); k++)
        {
            if(src[i].idx == pick[k]) {is_eq=true;break;}

        }
        if(is_eq)
        {
            dst.push_back(src[i]);
        }
    }
    */
    for(int i = 0; i<pick.size(); i++)
    {
        dst.push_back(src_[pick[i]]);
    }
    //cout << src[I[484]].area << "\n";
}



